import React from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { MdMenu, MdClear } from 'react-icons/lib/md'

import { toggleHeaderMenu } from 'api/toggleApi'

const navigation = [
    { title: 'Home', link: '/', color: '' },
    { title: 'About', link: '/about', color: '' },
];

class Header extends React.Component {
    render() {
        const headerMenu = (
            <div id="ci-header-menu">
                ABCD
            </div>
        )

        return (
            <div id="ci-header" className="p0_16">
                <div className="header-logo">CODE INTEREST</div>
                <MediaQuery minWidth={768} className="header-navigation">
                    {
                        navigation.map((item, index) => (
                            <div key={index} className="header-navigation-item">
                                <Link to={item.link}>{item.title}</Link>
                            </div>
                        ))
                    }
                    <div className="header-auth"></div>
                </MediaQuery>
                <MediaQuery maxWidth={767} className="header-navigation nav-button">
                    <div onClick={this.toggleHeaderMenu}>{!this.props.headerMenuVisible ? <MdMenu /> : <MdClear />}</div>
                    {this.props.headerMenuVisible && headerMenu}
                </MediaQuery>
            </div>
        )
    }

    toggleHeaderMenu = () => {
        this.props.toggleHeaderMenu();
    }
}

function mapStateToProps(state) {
    return {
        headerMenuVisible: state.toggleReducer.headerMenuVisible
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        toggleHeaderMenu
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)