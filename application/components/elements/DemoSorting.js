import React from 'react'
import ReactDOM from 'react-dom'
import Helmet from 'react-helmet'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchDataDemo } from 'api/exampleApi'

const listA = [
    {
        name: "Group 1",
        data: [1, 2, 3, 4, 5, 6, 7], // eslint-disable-line
    },
    {
        name: "Group 2",
        data: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17], // eslint-disable-line
    },
];
const listB = [
    {
        name: "Group A",
        data: [1, 4, 8, 14], // eslint-disable-line
    },
    {
        name: "Group B",
        data: [5, 12, 2, 3, 13], // eslint-disable-line
    },
    {
        name: "Group C",
        data: [6, 9, 7, 11, 10], // eslint-disable-line
    },
];
const listC = [
    {
        name: "Group A1",
        data: [1, 4], // eslint-disable-line
    },
    {
        name: "Group B1",
        data: [6, 9, 7, 11, 10, 15, 16, 2, 3, 5, 13, 17, 8, 14], // eslint-disable-line
    },
    {
        name: "Group C1",
        data: [12], // eslint-disable-line
    },
]
const allData = [
    {
        name: "",
        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17] // eslint-disable-line
    }
]

class DemoSorting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultList: allData
        }
    }
    componentDidMount() {
        this.sort(allData, listA);
    }
    render() {
        //const defaultList = allData;
        const defaultList = this.state.defaultList;

        return (
            <div id="ci-sorting">
                <Helmet title='Sorting' />
                <button onClick={() => this.sort(defaultList, listA)}>Sort A</button>
                <button onClick={() => this.sort(defaultList, listB)}>Sort B</button>
                <button onClick={() => this.sort(defaultList, listC)}>Sort C</button>
                <button onClick={this.doSomething}>Click me</button>
                messageHome: {this.props.state.data.message}<br />
                <div id="ci-sorting-list" ref="item_ref_list">
                    {
                        defaultList.map(list => (
                            <div key={list.name} className="ci-sorting-group">
                                {/* <div className="ci-sorting-group-title">{list.name}</div> */}
                                {list.data.map(item => (
                                    <div key={`item_${item}`} ref={`item_ref_${item}`} id={item} style={{ display: "inline-block", width: "50px", height: "50px", lineHeight: "50px", textAlign: "center", border: "2px solid black", margin: "5px", transition: "all 0.5s" }}>
                                        {item}
                                    </div>
                                ))}
                            </div>
                        ))
                    }
                </div>
            </div>
        )
    }

    /* eslint-disable */
    sort = (currentList, nextList) => {
        const width = ReactDOM.findDOMNode(this.refs[`item_ref_list`]).getBoundingClientRect().width;
        let cols_1 = 0;
        let cols_2 = 0;

        currentList.map((list_1, id1) => {
            list_1.data.map((item_1, id_1) => {
                cols_2 = 0;
                this.refs[`item_ref_${item_1}`].style.visibility = 'hidden';
                this.refs[`item_ref_${item_1}`].style.opacity = 0;
                nextList.map((list_2, id2) => {
                    list_2.data.map((item_2, id_2) => {
                        if (item_1 == item_2) {
                            let y = -(id_2 % 6 - id_1 % 6) * 64;
                            const widthCol1 = width / currentList.length;
                            const widthCol_1 = widthCol1 / Math.ceil(list_1.data.length / 6);
                            const widthCol2 = width / nextList.length;
                            const widthCol_2 = widthCol2 / Math.ceil(list_2.data.length / 6);

                            let x2 = (widthCol2 * id2) + (widthCol_2 * Math.floor(id_2 / 6)) + (widthCol_2 / 2);
                            let x1 = (widthCol1 * id1) + (widthCol_1 * Math.floor(id_1 / 6)) + (widthCol_1 / 2);
                            let x = x2 - x1;
                            // console.log(item_1, "_", id1, "_", id_1, " => ", id2, "_", id_2, ":", x1, x2, widthCol1, widthCol_1, widthCol2, widthCol_2);
                            // console.log(widthCol1 * id1, widthCol_1 * (Math.ceil(list_1.data.length / 6) - 1), widthCol_1 / 2)

                            this.refs[`item_ref_${item_1}`].style.visibility = 'visible';
                            this.refs[`item_ref_${item_1}`].style.opacity = 1;
                            this.refs[`item_ref_${item_1}`].style.transform = `translate(${x}px, ${y}px)`;
                        }
                        return null;
                    })
                    cols_2 += Math.floor(list_2.data.length / 6);
                })
            })
            cols_1 += Math.floor(list_1.data.length / 6);
            return null;
        })
    }

    doSomething = () => {
        this.props.fetchDataDemo(1);
    }
}

function mapStateToProps(state) {
    return {
        state: state.exampleReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDataDemo
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DemoSorting)