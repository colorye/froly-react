import React from 'react'
import MediaQuery from 'react-responsive'

import { Row, Col } from 'antd'

class Footer extends React.Component {
    render() {
        return (
            <div id="ci-footer">
                <Row className="footer-social">Social</Row>
                <Row>
                    <Col>Logo</Col>
                    <Col>Routes</Col>
                </Row>
                <div>Copyright</div>
            </div>
        )
    }
}

export default Footer