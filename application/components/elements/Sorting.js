import React from 'react'

class Arrange extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            data,
            dataMapping
        } = this.props;

        return (
            <div id="ci-sorting" ref="item-ci-sorting">
                {
                    data.map((item, index) => (
                        <div id="ci-sorting-item" key={`item-${item}`} ref={`item-ref-${item}`}>
                            {item}
                        </div>
                    ))}
                }
            </div>
        )
    }
}