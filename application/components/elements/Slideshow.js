import React from 'react'

import { Carousel } from 'antd'

import homeBanner from 'resources/img/home-banner.jpg'

const settings = {
    dots: false,
    arrows: true,
    infinite: true,
    adaptiveHeight: true,
    //autoplay: true,
    swipe: true,
    swipeToSlide: true
};

const responsive = [
    {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    },
    {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
        }
    },
    {
        breakpoint: 10000,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
        },
    },
];

const defaultHeight = 400;
class Slideshow extends React.Component {

    render() {
        const {
            height = defaultHeight,
            type = 'img',
            data = null
        } = this.props;

        return (
            <div id="ci-slideshow">
                {
                    data &&
                    <Carousel {...settings}>
                        {type === 'img' && data.map((item, index) => (
                            <div key={index} style={{ height: `${height}px`, lineHeight: `${height}px` }}>
                                <img src={homeBanner} title={item.title} alt={item.title} />
                            </div>
                        ))}
                    </Carousel>
                }
            </div>
        )
    }
}

export default Slideshow