import React from 'react'
import Helmet from 'react-helmet'

class About extends React.Component {
    render() {
        return (
            <div>
                <Helmet title='About' />
                This is about pages.
            </div>
        )
    }
}

export default About