import React from 'react'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Row, Col } from 'antd'

import Slideshow from 'components/elements/Slideshow'
import { MdArrowForward } from 'react-icons/lib/md'
import { fetchDataDemo } from 'api/exampleApi'

import { HomeBannerSlide } from 'virtual_data'
import homeBanner from 'resources/img/home-banner.jpg'

const HomeBannerSection = ({
    title = '',
    backgroundUrl = '',

}) => {
    return (
        <div className="banner-full-screen banner-centering bg-parallax" style={{ backgroundImage: `url(${backgroundUrl})` }}>
            <div className="banner-text-wrapper">
                <div className="title-extra">Home section</div>
                <div className="description">Code Interest - sharing everything</div>
                <div className="button-fancy">
                    <Link to="/about">Read more</Link>
                    <MdArrowForward className="right" />
                </div>
            </div>
        </div>
    )
}

const HomeFeature = () => (
    <Row gutter={8}>
        <Col md={16}><Slideshow height={300} data={HomeBannerSlide} /></Col>
        <Col md={8}>
            <img src="https://dummyimage.com/600x200/000/fff" width="100%" />
            <img src="https://dummyimage.com/600x200/000/fff" width="100%" />
        </Col>
    </Row>
)

const HomeCategories = () => (
    <div className="home-categories">
        <div className="home-categories-item">Làm đẹp</div>
        <div className="home-categories-item">Sức khoẻ</div>
    </div>
)

const HomeGrid = () => (
    <div className="home-grid">

    </div>
)

class Home extends React.Component {
    render() {
        return (
            // <div>
            //     <Helmet title='Home' />
            //     Home page.<br />
            //     messageHome: {state.data.message}<br />
            //     <button onClick={this.buttonOnClick}>Click me! {state.isLoading ? 'Loading' : ''}</button>
            // </div>
            <div id="ci-section-home">
                <Helmet title='Home' />
                <HomeFeature />
            </div>
        )
    }

    buttonOnClick = () => {
        this.props.fetchDataDemo(1);
    }
}

function mapStateToProps(state) {
    return {
        state: state.exampleReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDataDemo
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
