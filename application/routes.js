import React from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import Helmet from 'react-helmet'

import createBrowserHistory from 'history/createBrowserHistory'

import Header from 'components/elements/Header'

import Home from 'components/pages/home/Home.react'
import About from 'components/pages/about/About.react'
import DemoSorting from 'components/elements/DemoSorting'

const history = createBrowserHistory();
const brandName = 'Froly';

const indexRoutes = (
    <div id="ci-index-routes">
        <Header />
        <div className="p0_8">
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/sorting" component={DemoSorting} />
            </Switch>
        </div>
    </div>
)

const routes = (
    <div id="ci-main-wrapper" className="mw1440">
        <Helmet titleTemplate={`%s - ${brandName}`} />
        <Switch>
            {indexRoutes}
        </Switch>
    </div>
)

class App extends React.Component {
    render() {
        return (
            <Router history={history}>{routes}</Router>
        );
    }
}

export default App