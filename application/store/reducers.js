import { combineReducers } from 'redux'

import exampleReducer from './reducers/exampleReducer'
import toggleReducer from './reducers/toggleReducer'

const reducers = combineReducers({
    exampleReducer,
    toggleReducer
});

export default reducers