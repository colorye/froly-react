import * as constants from 'actions/constants'

const initialState = {
    headerMenuVisible: false,
}

const handlers = {
    [`${constants.TOGGLE_HEADER_MENU}`]: (state, action) => {
        return {
            ...state,
            headerMenuVisible: !state.headerMenuVisible
        };
    },
}

const exampleReducer = (state = initialState, action) => {
    if (handlers[action.type]) {
        return handlers[action.type](state, action);
    }
    return state;
}

export default exampleReducer