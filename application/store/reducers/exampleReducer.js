import * as constants from 'actions/constants'

const initialState = {
    isLoading: false,
    data: {
        message: 'Hello redux'
    }
}

const handlers = {
    [`${constants.HOME_FETCH_DATA}_REQUEST`]: (state, action) => {
        return {
            ...state,
            isLoading: action.isLoading,
        };
    },
    [`${constants.HOME_FETCH_DATA}_SUCCESS`]: (state, action) => {
        return {
            ...state,
            isLoading: action.isLoading,
            data: action.response.data,
        };
    },
    [`${constants.HOME_FETCH_DATA}_ERROR`]: (state, action) => {
        return {
            ...state,
            isLoading: action.isLoading,
            data: action.response.data
        };
    },
}

const exampleReducer = (state = initialState, action) => {
    if (handlers[action.type]) {
        return handlers[action.type](state, action);
    }
    return state;
}

export default exampleReducer