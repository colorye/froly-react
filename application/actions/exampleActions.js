import * as constants from './constants'

export function fetchDataRequest() {
    return dispatch => {
        dispatch({
            type: `${constants.HOME_FETCH_DATA}_REQUEST`,
            isLoading: true
        })
    }
}

export function fetchDataSuccess(data) {
    return dispatch => {
        dispatch({
            type: `${constants.HOME_FETCH_DATA}_SUCCESS`,
            isLoading: false,
            response: data
        })
    }
}

export function fetchDataError(data) {
    return dispatch => {
        dispatch({
            type: `${constants.HOME_FETCH_DATA}_ERROR`,
            isLoading: false,
            response: data
        })
    }
}