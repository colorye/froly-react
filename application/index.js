import React from 'react'
import { render } from 'react-snapshot'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'


import registerServiceWorker from './registerServiceWorker'
import reducers from 'store/reducers'

import App from './routes'
import 'decorators/main.css'



// Integrate redux devtools
const store = createStore(reducers, composeWithDevTools(
    applyMiddleware(thunk)
));

render((
    <Provider store={store}>
        <App />
    </Provider>
), document.getElementById('project-froly'));

registerServiceWorker();

if (module.hot) {
    module.hot.accept('./routes', () => {
        const NextApp = require('./routes').default
        render(
            <Provider store={store}>
                <NextApp />
            </Provider>,
            document.getElementById('project-froly')
        )
    })
}