import * as constants from 'actions/constants'

export function toggleHeaderMenu() {
    return dispatch => {
        dispatch({
            type: `${constants.TOGGLE_HEADER_MENU}`
        })
    }
}