//import { serverUrl } from 'config'

//import $ from 'jquery'

import * as actions from 'actions/exampleActions'

export function fetchDataDemo(id) {
    return dispatch => {
        dispatch(actions.fetchDataRequest());
        // $.ajax({
        //     type: 'GET', // 'POST'
        //     url: `${serverUrl}/api/fetch`,
        //     headers: {

        //     },
        //     success: function (response) {
        //         dispatch(actions.fetchDataSuccess({
        //             data: response
        //         }))
        //     },
        //     error: function (response) {
        //         dispatch(actions.fetchDataError({
        //             data: response
        //         }))
        //     }
        // })

        dispatch(actions.fetchDataSuccess({
            data: {
                state: 'SUCCESS',
                message: 'Success fetch data'
            }
        }));
    }
}